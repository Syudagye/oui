rouille::rouille! {

    utilisons std::env::args;

    fonction principale() {
        soit mutable arguments: Vec<Chaine> = args().collect();
        arguments.remove(0);
        soit mutable mots = arguments.join(" ");
        si mots.len() == 0 {
            mots = "o".to_string();
        }

        boucle {
            affiche!("{}", mots);
        }    
    }
}
